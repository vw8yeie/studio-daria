document.addEventListener('polymer-ready', function() {
  var navicon = document.getElementById('navicon');
  var drawerPanel = document.getElementById('drawerPanel');
  navicon.addEventListener('click', function() {
    drawerPanel.togglePanel();
  });
});

var preise = {};
preise.medfusspflege = 28;
preise.pedfusspflege = 20;
preise.fussreflexmassage = 35;
preise.nagelaufbau = 30;
preise.fusspflege = {};
preise.fusspflege.extras = {};
preise.fusspflege.extras.parafin = '+10';
preise.fusspflege.extras.nagellack = '+5';
preise.fusspflege.extras.uvlack = '+20';
preise.fusspflege.extras.frenchmodellage ='+20';

var fussTmpl = document.querySelector('#fussTmpl');
if (fussTmpl) {
    fussTmpl.medfusspflege = preise.medfusspflege;
    fussTmpl.pedfusspflege = preise.pedfusspflege;
    fussTmpl.fussreflexmassage = preise.fussreflexmassage;
    fussTmpl.nagelaufbau = preise.nagelaufbau;
    fussTmpl.fusspflege = {};
    fussTmpl.fusspflege.extras = {};
    fussTmpl.fusspflege.extras.parafin = preise.fusspflege.extras.parafin;
    fussTmpl.fusspflege.extras.nagellack = preise.fusspflege.extras.nagellack;
    fussTmpl.fusspflege.extras.uvlack = preise.fusspflege.extras.uvlack;
    fussTmpl.fusspflege.extras.frenchmodellage = preise.fusspflege.extras.frenchmodellage;
}
