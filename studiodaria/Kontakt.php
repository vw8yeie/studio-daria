
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>Kontakt</title>
	
	<link type="text/css" rel="stylesheet" href="masterroot.css" />
	
	<link type="text/css" rel="stylesheet" href="tables.css" />
	
	
	<link type="text/css" rel="stylesheet" href="theme.css" />
	
</head>
<body>

<form name="aspnetForm" method="post" action="Kontakt.php" id="phpForm">

	<table cellspacing="0" class="MS_MasterFrame" cellpadding="0" border="0">
		<tr >
			<td id="IWS_WH_Elem_Header" class="MS_MasterHeader">
				<TABLE cellSpacing="0" cellPadding="0" width="780" border="0" class="MSC_SiteWidth">
  <TR>
    <TD width="147" class="BG_Light"><img  height="131" src="Bilder/lotus.jpg" /></TD>
    <td class="BG_Light" valign="middle" align="left" width="65%" style="padding:0 0 0 20">
      <table cellpadding="0" cellspacing="0" border="0">
        <tr>
          <td align="left">
            <h1 id="IWS_WH_Elem_SiteTitle" class="MSC_HeaderText F_Dark" style="padding:0px;margin:0px"><span style="font-weight: normal; font-size: 24pt; color: #ffffff; font-style: normal; font-family: Arial; text-decoration: ">Studio Daria</span></h1>
          </td>
        </tr>
        <tr>
          <td align="left">
            <h4 id="IWS_WH_Elem_SiteDescription" class="MSC_HeaderDescription F_Dark"><span style="font-weight: normal; font-size: 14pt; color: #ffffff; font-style: normal; font-family: Arial; text-decoration: ">. . . die reiche Welt der Kosmetik</span></h4>
          </td>
        </tr>
      </table>
    </td>
  </TR>
</TABLE>
			</td>
		</tr>
		<tr> 
			<td>
			<table cellspacing="0" cellpadding="0" border="0" style="width:100%"> <tr>
				<td id="IWS_WH_Elem_LeftNav" nowrap="true" class="MS_MasterLeftNav" style="width:150px" >
					<table cellpadding="0" cellspacing="0" class="MSC_PrimaryNavFrame">
                      <tr>
                        <td class="MSC_PrimaryNavLinkFrame"><a href="&#xA;                  ./index.htm" class="MSC_PrimaryNavLink">Startseite</a></td>
                      </tr>
                      <tr>
                        <td class="MSC_PrimaryNavTopSpace"></td>
                      </tr>
                      <tr>
                        <td class="MSC_PrimaryNavBottomSpace"></td>
                      </tr>
                      <tr>
                        <td class="MSC_PrimaryNavLinkFrame"><a href="&#xA;                  ./aboutus.htm" class="MSC_PrimaryNavLink">Wir &uuml;ber uns/Bilder</a></td>
                      </tr>
                      <tr>
                        <td class="MSC_PrimaryNavTopSpace"></td>
                      </tr>
                      <tr>
                        <td class="MSC_PrimaryNavBottomSpace"></td>
                      </tr>
                      <tr>
                        <td class="MSC_PrimaryNavLinkFrame"><a href="&#xA;                  ./Massagen.htm" class="MSC_PrimaryNavLink">Massagen</a></td>
                      </tr>
                      <tr>
                        <td class="MSC_PrimaryNavTopSpace"></td>
                      </tr>
                      <tr>
                        <td class="MSC_PrimaryNavBottomSpace"></td>
                      </tr>
                      <tr>
                        <td class="MSC_PrimaryNavLinkFrame"><a href="&#xA;                  ./Kosmetik.htm" class="MSC_PrimaryNavLink">Kosmetik</a></td>
                      </tr>
                      <tr>
                        <td class="MSC_PrimaryNavTopSpace"></td>
                      </tr>
                      <tr>
                        <td class="MSC_PrimaryNavBottomSpace"></td>
                      </tr>
                      <tr>
                        <td class="MSC_PrimaryNavLinkFrame"><a href="&#xA;                  ./Fusspflege.htm" class="MSC_PrimaryNavLink">Fu&szlig;pflege</a></td>
                      </tr>
                      <tr>
                        <td class="MSC_PrimaryNavTopSpace"></td>
                      </tr>
                      <tr>
                        <td class="MSC_PrimaryNavBottomSpace"></td>
                      </tr>
                      <tr>
                        <td class="MSC_PrimaryNavLinkFrame"><a href="&#xA;                  ./Wimpern.htm" class="MSC_PrimaryNavLink">Wimpern / Haare</a></td>
                      </tr>
                      <tr>
                        <td class="MSC_PrimaryNavTopSpace"></td>
                      </tr>
                      <tr>
                        <td class="MSC_PrimaryNavBottomSpace"></td>
                      </tr>
                      <tr>
                        <td class="MSC_PrimaryNavLinkFrame"><a href="&#xA;                  ./aura_soma.htm" class="MSC_PrimaryNavLink">Aura Soma</a></td>
                      </tr>
                      <tr>
                        <td class="MSC_PrimaryNavTopSpace"></td>
                      </tr>
                      <tr>
                        <td class="MSC_PrimaryNavBottomSpace"></td>
                      </tr>
                      <tr>
                        <td class="MSC_PrimaryNavLinkFrame-On"><a href="&#xA;                  ./Kontakt.php" class="MSC_PrimaryNavLink-On">Kontakt</a></td>
                      </tr>
                      <tr>
                        <td class="MSC_PrimaryNavTopSpace"></td>
                      </tr>
                      <tr>
                        <td class="MSC_PrimaryNavBottomSpace"></td>
                      </tr>
                      <tr>
                        <td class="MSC_PrimaryNavLinkFrame"><a href="&#xA;                  ./Preise.htm" class="MSC_PrimaryNavLink">Preisliste</a></td>
                      </tr>
                      <tr>
                        <td class="MSC_PrimaryNavTopSpace"></td>
                      </tr>
                      <tr>
                        <td class="MSC_PrimaryNavBottomSpace"></td>
                      </tr>
                      <tr>
                        <td class="MSC_PrimaryNavLinkFrame"><a href="&#xA;                  ./Impressum.htm" class="MSC_PrimaryNavLink">Impressum</a></td>
                      </tr>
                      <tr>
                        <td class="MSC_PrimaryNavTopSpace"></td>
                      </tr>
                      <tr>
                        <td class="MSC_PrimaryNavBottomSpace"></td>
                      </tr>
                    </table>
					
				</td>
				<td class="MS_MasterBody">
					<div id="IWS_WH_Elem_Content" class="MSC_Body">
                    <?php
                        if (isset($_POST["Senden"])) {
                            $email = $_POST["eMailAddress"];
                            $betreff = $_POST["Betreff"];
                            $nachricht = $_POST["Nachricht"];

                             # Ihre E-Mail-Adresse
                             $an ="info@studiodaria.de";

                             # Diese Nachricht wird an Ihre E-Mail-Adresse gesendet
                             $text = "Hallo,\n Sie haben eine neue Nachricht von Studio Daria erhalten:\n\n
                             ====\n $nachricht\n====\n\nSie k&ouml;nnen die Kontaktperson unter dieser $email erreichen.";
                             @mail($an, $betreff, $text, "From: " . $email);
                             echo "<p><b>Ihre Nachricht wurde gesendet!</b></p>";
                             echo "<input id='Ok' type='submit' name='Ok' value='Ok' />";
                             echo "<br/>";
                        }
                        else
                        {
                    ?>
				          <p>
				            Falls Sie Fragen haben, schicken Sie mir bitte Ihre Nachricht mit dem unten angegebenen Formular oder direkt per E-Mail. Ich werde mich dann umgehend bei Ihnen melden.
				          </p>	
                          <br/> 
                          <p>
                            <strong>Studio Daria</strong>
                          </p>
						 <p>
							Mandelnstra&szlig;e 2	 
						 </p>
                          <p style="text-decoration: underline">
                            38100 Braunschweig
                          </p>
                          <p>
                            Telefon: 015114113214
                          </p>
                          <p>
                            E-Mail: info@studiodaria.de
                          </p>
                          <br />
                          <p>
                            Ihre E-Mail Adresse:
                          </p> 
                          <p>
                            <input id="eMailAddress" name="eMailAddress" type="text" />
                          </p>
                          <br />
                          <p>
                            Betreff:
                          </p>
                          <p>
                            <input id="Betreff" name="Betreff" type="text" />
                          </p>
                          <br />
                          <p>
                            Ihre Nachricht;
                          </p>
                          <p>
                            <textarea id="Nachricht" name="Nachricht" cols="40" rows="10"></textarea>
                          </p>
                          <br />
                          <p>
                            <input id="Submit" type="submit" name="Senden" value="Absenden" />
                          </p>
                        <?php
                        } ?>
                      
					</div>
				</td>
			</tr></table>
			</td>
		</tr>
		

	</table>
</form>

</body>
</html>
