<!doctype html>
<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="apple-mobile-web-app-capable" content="yes">
  <meta name="mobile-web-app-capable" content="yes">
  <title>Kosmetikstudio Daria Braunschweig</title>
    <link href="../css/app.css" rel="stylesheet"/>
    <script src="../bower_components/webcomponentsjs/webcomponents.js"></script>
    <link rel="import" href="../bower_components/core-drawer-panel/core-drawer-panel.html">
    <link rel="import" href="../bower_components/core-header-panel/core-header-panel.html">
    <link rel="import" href="../bower_components/core-toolbar/core-toolbar.html">
    <link rel="import" href="../bower_components/core-menu/core-menu.html">
    <link rel="import" href="../bower_components/core-item/core-item.html">
    <link rel="import" href="../bower_components/core-icon-button/core-icon-button.html">


</head>
<body fullbleed layout vertical>
    <core-drawer-panel id="drawerPanel">
         <core-header-panel mode="waterfall-tall" tallClass="medium-tall" drawer>
             <core-toolbar><img  height="131" src="../Bilder/lotus.jpg" /></core-toolbar>
             <core-menu selected="7">
                 <core-item label="Startseite">
                     <a href="../index.html"></a>
                 </core-item>
                 <core-item label="Wir &uuml;ber uns/Bilder">
                     <a href="aboutus.html"></a>
                 </core-item>
                 <core-item label="Massagen">
                     <a href="massagen.html"></a>
                 </core-item>
                 <core-item label="Kosmetik">
                     <a href="kosmetik.html"></a>
                 </core-item>
                 <core-item label="Fu&szlig;pflege">
                    <a href="fusspflege.html"></a>
                 </core-item>
                 <core-item label="Wimpern / Haare">
                     <a href="wimpern.html"></a>
                 </core-item>
                 <core-item label="Aura Soma">
                     <a href="aura_soma.html"></a>
                 </core-item>
                 <core-item label="Kontakt"></core-item>
                 <core-item label="Preisliste">
                    <a href="preise.html"></a>
                  </core-item>
                 <core-item label="Impressum">
                     <a href="impressum.html"></a>
                 </core-item>
             </core-menu>
        </core-header-panel>
       
        <core-header-panel main>
            <core-toolbar class="medium-tall">
                <core-icon-button id="navicon" icon="menu">
                </core-icon-button>
                <h1>Studio Daria</h1>
                <div class="bottom">...die reiche Welt der Kosmetik</div>
            </core-toolbar>
            <div class="content">
                <form name="phpForm" method="post" action="kontakt.php" id="phpForm">
                    <?php
                        if (isset($_POST["Senden"])) {
                            $email = $_POST["eMailAddress"];
                            $betreff = htmlspecialchars($_POST["Betreff"]);
                            $nachricht = htmlspecialchars($_POST["Nachricht"]);

                             # Ihre E-Mail-Adresse
                             $an ="info@studiodaria.de";

                             # Diese Nachricht wird an Ihre E-Mail-Adresse gesendet
                             $text = "Hallo,\n Sie haben eine neue Nachricht von Studio Daria erhalten:\n\n
                              $nachricht\n\nSie können die Kontaktperson unter dieser e-Mail Adresse $email erreichen.";
                             @mail($an, $betreff, $text, "From: " . $email);
                             echo "<p><b>Ihre Nachricht wurde gesendet!</b></p>";
                             echo "<input id='Ok' type='submit' name='Ok' value='Ok' />";
                             echo "<br/>";
                        }
                        else
                        {
                    ?>
				          <p>
				            Falls Sie Fragen haben, schicken Sie mir bitte Ihre Nachricht mit dem unten angegebenen Formular oder direkt per E-Mail. Ich werde mich dann umgehend bei Ihnen melden.
				          </p>	
                          <br/> 
                          <p>
                            <strong>Studio Daria</strong>
                          </p>
						 <p>
							Mandelnstra&szlig;e 2	 
						 </p>
                          <p style="text-decoration: underline">
                            38100 Braunschweig
                          </p>
                          <p>
                            Telefon: 015114113214
                          </p>
                          <p>
                            E-Mail: info@studiodaria.de
                          </p>
                          <br />
                          <p>
                            Ihre E-Mail Adresse:
                          </p> 
                          <p>
                            <input id="eMailAddress" name="eMailAddress" type="text" />
                          </p>
                          <br />
                          <p>
                            Betreff:
                          </p>
                          <p>
                            <input id="Betreff" name="Betreff" type="text" />
                          </p>
                          <br />
                          <p>
                            Ihre Nachricht;
                          </p>
                          <p>
                            <textarea id="Nachricht" name="Nachricht" cols="40" rows="10"></textarea>
                          </p>
                          <br />
                          <p>
                            <input id="Submit" type="submit" name="Senden" value="Absenden" />
                          </p>
                        <?php
                        } ?>                
                </form>
            </div>
        </core-header-panel>
    </core-drawer-panel>
    
<script src="../js/app.js"></script>
</body>
</html>